package engine;

import java.util.ArrayList;

public abstract class Lyrics {
	
	

	public static String match(ArrayList<String> notes,ArrayList<String> lyrics)
	{
		String result = "";
		int counter = 0;
		for (String each:notes)
		{
			if (counter < lyrics.size())
				result += each.replace("E \"lyric \"", "E \"lyric " + lyrics.get(counter).trim() + "\"") + "\n";
			else
				result += each + "\n";
			if (each.contains("E \"lyric \""))
				counter++;
		}
		
		return result;
	}
}
