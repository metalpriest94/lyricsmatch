package engine;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;

public class Main extends JFrame {

	private JPanel contentPane;
	private JTextPane textPaneNotes;
	private JTextPane textPaneLyrics;
	private JTextPane textPaneOutput;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 642, 426);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow][grow][grow]", "[][grow][][][]"));
		
		JLabel lblNotes = new JLabel("Paste notes.chart here");
		contentPane.add(lblNotes, "cell 0 0,growx");
		
		JLabel lblLyrics = new JLabel("Paste lyrics here (one syllable per line)");
		contentPane.add(lblLyrics, "cell 1 0,growx");
		
		JLabel lblCopyThisInto = new JLabel("Copy this into notes.chart");
		contentPane.add(lblCopyThisInto, "cell 2 0");
		
		JScrollPane scrollPaneNotes = new JScrollPane();
		contentPane.add(scrollPaneNotes, "cell 0 1,grow");
		
		textPaneNotes = new JTextPane();
		scrollPaneNotes.setViewportView(textPaneNotes);
		textPaneNotes.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		JScrollPane scrollPaneLyrics = new JScrollPane();
		contentPane.add(scrollPaneLyrics, "cell 1 1,grow");
		
		textPaneLyrics = new JTextPane();
		scrollPaneLyrics.setViewportView(textPaneLyrics);
		textPaneLyrics.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		JButton btnMatch = new JButton("Match");
		btnMatch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textPaneOutput.setText(Lyrics.match(readPanel(textPaneNotes),readPanel(textPaneLyrics)));
			}
		});
		
		JScrollPane scrollPaneOutput = new JScrollPane();
		contentPane.add(scrollPaneOutput, "cell 2 1,grow");
		
		textPaneOutput = new JTextPane();
		textPaneOutput.setBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPaneOutput.setViewportView(textPaneOutput);
		
		JButton btnPreformat = new JButton("Preformat lyrics");
		contentPane.add(btnPreformat, "cell 0 2 3 1,growx");
		
		JSeparator separator = new JSeparator();
		contentPane.add(separator, "cell 0 3 3 1,grow");
		contentPane.add(btnMatch, "cell 0 4 3 1,growx");
	}
	
	public ArrayList<String> readPanel(JTextPane panel)
	{
		ArrayList<String> result = new ArrayList<String>();
		for (String each : panel.getText().split("\r\n"))
		{
			result.add(each);
		}
		return result;
	}
	
}
